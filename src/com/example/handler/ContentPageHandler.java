package com.example.handler;

import java.util.ArrayList;
import java.util.Observable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.model.Catagory;
import com.example.model.Content;

public class ContentPageHandler extends Observable implements WebDataProcessorInterface{
	ArrayList<Content> contentList;
	private static ContentPageHandler instance = null;

	public ArrayList<Content> getContentList() {
		return contentList;
	}

	public void setContentList(ArrayList<Content> contentList) {
		this.contentList = contentList;
	}
	
	private ContentPageHandler(){}
	
	public static ContentPageHandler getInstance(){
		if(instance == null)
			instance = new ContentPageHandler();
		return instance;
	}

	@Override
	public void processData(String jsonString) {
		contentList = new ArrayList<Content>();
		try {
			JSONObject obj = new JSONObject(jsonString);
			JSONArray data = obj.getJSONArray("data");
			for (int i = 0; i < data.length(); i++) {
				JSONObject jObj = data.getJSONObject(i);
				String catagoryName = jObj.getString("title");
				String url = jObj.getString("url");
				Content content = new Content(catagoryName, url);
				contentList.add(content);
			}
			setChanged();
			notifyObservers(this);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
