package com.example.handler;

import java.util.ArrayList;
import java.util.Observable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.model.Article;
import com.example.model.BottomItem;
import com.example.model.Catagory;

public class MainPageHandler extends Observable implements WebDataProcessorInterface{
	private static MainPageHandler instance = null;
	
	ArrayList<Catagory> catagoryList;
	ArrayList<BottomItem> bottomList;
	ArrayList<Article>articleList;
	String jsonString;
	
	public ArrayList<BottomItem> getBottomList() {
		return bottomList;
	}
	public void setBottomList(ArrayList<BottomItem> bottomList) {
		this.bottomList = bottomList;
	}
	public ArrayList<Article> getArticleList() {
		return articleList;
	}
	public void setArticleList(ArrayList<Article> articleList) {
		this.articleList = articleList;
	}
	public ArrayList<Catagory> getCatagoryList() {
		return catagoryList;
	}
	public void setCatagoryList(ArrayList<Catagory> catagoryList) {
		this.catagoryList = catagoryList;
	}
	
	public static MainPageHandler getInstance(String jsonString){
		if(instance == null){
			instance = new MainPageHandler(jsonString);
		}
		instance.jsonString = jsonString;
		return instance;
	}
	private MainPageHandler(){}
	private MainPageHandler(String jsonString) {
		super();
		this.jsonString = jsonString;
	}
	
	public void processData(String jsonString){
		this.jsonString = jsonString;
		catagoryList = new ArrayList<Catagory>();
		bottomList = new ArrayList<BottomItem>();
		articleList = new ArrayList<Article>();
		
		try {
			JSONObject obj = new JSONObject(jsonString);
			JSONArray data = obj.getJSONArray("data");
			JSONArray data1 = obj.getJSONArray("data1");
			JSONArray data2 = obj.getJSONArray("data2");
			
			for (int i = 0; i < data.length(); i++) {
				JSONObject jObj = data.getJSONObject(i);
				String catagoryName = jObj.getString("catagory");
				String url = jObj.getString("url");
				Catagory catagory = new Catagory(catagoryName, url);
				catagoryList.add(catagory);
			}

			for(int i = 0; i < data1.length(); i++){
				JSONObject jObj = data1.getJSONObject(i);
				String title = jObj.getString("title");
				String url = jObj.getString("url");
				Article article = new Article(title, url);
				articleList.add(article);
			}
			
			for(int i = 0; i < data2.length(); i++){
				JSONObject jObj = data2.getJSONObject(i);
				String title = jObj.getString("next");
				String url = jObj.optString("url",null);
				BottomItem item = new BottomItem(title, url);
				bottomList.add(item);
			}

			setChanged();
			notifyObservers(this);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
