package com.example.handler;


public class HandlerMapping {
	public WebDataProcessorInterface getHandlerFromUrl(String url){
		if(url.compareTo("/")==0){
			return MainPageHandler.getInstance(null);
		}
		String[] array = url.split("_");
		if(array.length == 2){
			return MainPageHandler.getInstance(null);
		}
		if(array.length == 3){
			return ContentPageHandler.getInstance();
		}
		return null;
	}
}
