package com.example.handler;

public interface WebDataProcessorInterface {
	void processData(String string);
}
