package com.example.model;

import android.webkit.JavascriptInterface;

import com.example.handler.HandlerMapping;
import com.example.handler.MainPageHandler;
import com.example.handler.WebDataProcessorInterface;

public class WebAppInterface {
	
	HandlerMapping mapping = new HandlerMapping();
	
	@JavascriptInterface
	public String getMsg(String string, String url) {
		System.out.println(string);
		
		WebDataProcessorInterface f = mapping.getHandlerFromUrl(url);
		if(f!=null){
			f.processData(string);
		}
		return string;
	}
}
