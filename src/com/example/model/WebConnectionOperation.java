package com.example.model;

import java.io.IOException;
import java.io.InputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebSettings;
import android.webkit.WebStorage.QuotaUpdater;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("SetJavaScriptEnabled")
public class WebConnectionOperation {
	private static WebConnectionOperation instance;
	private WebView webView;
	private Context context;
	private WebConfig config;
	private String jquery;
	private String currentUrl = null;
	private WebConnectionOperation(Context c, WebView webview) {
		context = c;
		webView = webview;
		config = new WebConfig();
		WebSettings wset = webView.getSettings();
		wset.setJavaScriptEnabled(true);
		webView.setWebViewClient(new myWebView());
		webView.setWebChromeClient(new WebChromeClient(){

			@Override
			public boolean onJsAlert(WebView view, String url, String message,
					JsResult result) {
				Log.d("Webconnection", "to here");
//				return super.onJsAlert(view, url, message, result);
				return true;
			}

			@Override
			public Bitmap getDefaultVideoPoster() {
				Log.d("Webconnection", "to here1");
				return super.getDefaultVideoPoster();
			}

			@Override
			public View getVideoLoadingProgressView() {
				Log.d("Webconnection", "to here2");
				return super.getVideoLoadingProgressView();
			}

			@Override
			public void getVisitedHistory(ValueCallback<String[]> callback) {
				Log.d("Webconnection", "to here3");
				super.getVisitedHistory(callback);
			}

			@Override
			public void onCloseWindow(WebView window) {
				Log.d("Webconnection", "to here4");
				super.onCloseWindow(window);
			}

			@Override
			public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
				Log.d("Webconnection", "to here5");
				return super.onConsoleMessage(consoleMessage);
			}



			@Override
			public boolean onCreateWindow(WebView view, boolean isDialog,
					boolean isUserGesture, Message resultMsg) {
				Log.d("Webconnection", "to here6");
				return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
			}



			@Override
			public void onHideCustomView() {
				Log.d("Webconnection", "to here7");
				super.onHideCustomView();
			}

			@Override
			public boolean onJsBeforeUnload(WebView view, String url,
					String message, JsResult result) {
				Log.d("Webconnection", "to here8");
				return super.onJsBeforeUnload(view, url, message, result);
			}

			@Override
			public boolean onJsConfirm(WebView view, String url,
					String message, JsResult result) {
				Log.d("Webconnection", "to here9");
				return super.onJsConfirm(view, url, message, result);
			}

			@Override
			public boolean onJsPrompt(WebView view, String url, String message,
					String defaultValue, JsPromptResult result) {
				Log.d("Webconnection", "to here10");
				return super.onJsPrompt(view, url, message, defaultValue, result);
			}


			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				if(currentUrl.startsWith("http://www.yi-see.com/")&&newProgress>50){
					String function = config.getFunction(currentUrl);
					if (function != null) {
						WebSettings wset = webView.getSettings();
						wset.setJavaScriptEnabled(true);
						webView.addJavascriptInterface(new WebAppInterface(), "Android");
						webView.loadUrl("javascript:" + jquery);
//						webView.loadUrl("javascript:window.Android.getMsg('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
						webView.loadUrl(function);
//						System.out.println(function);
					}
				}
				Log.d("Webconnection", "to here11");
				super.onProgressChanged(view, newProgress);
			}


			@Override
			public void onReceivedIcon(WebView view, Bitmap icon) {
				Log.d("Webconnection", "to here12");
				super.onReceivedIcon(view, icon);
			}

			@Override
			public void onReceivedTitle(WebView view, String title) {
				Log.d("Webconnection", "to here13");
				super.onReceivedTitle(view, title);
			}

			@Override
			public void onReceivedTouchIconUrl(WebView view, String url,
					boolean precomposed) {
				Log.d("Webconnection", "to here14");
				super.onReceivedTouchIconUrl(view, url, precomposed);
			}

			@Override
			public void onRequestFocus(WebView view) {
				Log.d("Webconnection", "to here15");
				super.onRequestFocus(view);
			}

			@Override
			public void onShowCustomView(View view, CustomViewCallback callback) {
				Log.d("Webconnection", "to here16");
				super.onShowCustomView(view, callback);
			}

			
		});
		jquery = LoadData("jquery.js");
		webView.addJavascriptInterface(new WebAppInterface(), "Android");
	}
	
	public static WebConnectionOperation getInstance(){
		return instance;
	}

	public static WebConnectionOperation getInstance(Context c, WebView webview) {
		if (instance == null) {
			instance = new WebConnectionOperation(c, webview);
		}
		return instance;
	}

	public void sendReqest(String url) {
		String absUrl = config.absUrl(url);
		webView.loadUrl(absUrl);
	}

	private class myWebView extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {
//				System.out.println("--->" + url);
//				String function = config.getFunction(url);
//				if (function != null) {
//					WebSettings wset = webView.getSettings();
//					wset.setJavaScriptEnabled(true);
//					webView.addJavascriptInterface(new WebAppInterface(), "Android");
//					webView.loadUrl("javascript:" + jquery);
////					webView.loadUrl("javascript:window.Android.getMsg('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
//					webView.loadUrl(function);
////					System.out.println(function);
//				}
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			currentUrl = url;
			super.onPageStarted(view, url, favicon);
		}
	}

	public String LoadData(String inFile) {
		String tContents = "";

		try {
			InputStream stream = context.getAssets().open(inFile);

			int size = stream.available();
			byte[] buffer = new byte[size];
			stream.read(buffer);
			stream.close();
			tContents = new String(buffer);
		} catch (IOException e) {
			// Handle exceptions here
		}
		return tContents;
	}

	private class WebConfig {
		public final static String baseUrl = "http://www.yi-see.com";
		private final static String mainPageJs = "javascript:(function () {var data = new Array();$('.Header').find('a').each(function () {data.push({'catagory': $(this).html(),'url':$(this).attr('href')});});var data1 = new Array();$('[class^=T] a').each(function(){data1.push({'title':$(this).html(),'url':$(this).attr('href')});});var data2 = new Array();$('.NEXT a').each(function(){data2.push({'next':$(this).html(),'url':$(this).attr('href')});});Android.getMsg(JSON.stringify({'data': data,'data1':data1,'data2':data2}),";
		private final static String secondPageJs = "javascript:(function () {var data = new Array();$('a[href^=read]').each(function () {data.push({'title': $(this).html(),'url': $(this).attr('href')});});Android.getMsg (JSON.stringify({'data': data}),";
		private final static String detailPageJs = "javascript:(function () {var data = new Array();$('.ART').each(function () {data.push({'txt': $(this).html(),});});var data1 = new Array();$('.B2 a').each(function(){data1.push({'title':$(this).html(),'url':$(this).attr('href')});});Android.getMsg(JSON.stringify({'data': data,'data1':data1}),";
		private final static String endcomments = ");})();";
		private final String delim = "_";

		public String getFunction(String absUrl) {
			if (!absUrl.startsWith(baseUrl))
				return null;
			String url = absUrl.substring(WebConfig.baseUrl.length());
			if (url.length() == 0 || url.compareTo("/") == 0)
				return mainPageJs+"'"+url+"'"+endcomments;
			String[] array = url.split(delim);
			if (array.length == 2)
				return mainPageJs+"'"+url+"'"+endcomments;
			if (array[0].compareToIgnoreCase("/art") == 0) {
				return secondPageJs+"'"+url+"'"+endcomments;
			} else {
				return detailPageJs+"'"+url+"'"+endcomments;
			}
		}

		public String absUrl(String url) {
			if (url.length() == 0 || url.compareTo("/") == 0) {
				return baseUrl;
			} else if(url.startsWith("/")){
				return baseUrl + url;
			}else{
				return baseUrl + "/" + url;
			}
		}
	}
}
