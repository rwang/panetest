package com.example.model;

public class Content {
	String chapter;
	public String getChapter() {
		return chapter;
	}
	public void setChapter(String chapter) {
		this.chapter = chapter;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	String url;
	public Content(String chapter, String url) {
		super();
		this.chapter = chapter;
		this.url = url;
	}
}
