package com.example.model;

public class BottomItem {
	String next;
	String url;

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public BottomItem(String next, String url) {
		super();
		this.next = next;
		this.url = url;
	}
}
