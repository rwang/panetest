package com.example.panetest;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.handler.MainPageHandler;
import com.example.model.Article;
import com.example.model.WebConnectionOperation;

public class SecondPane extends Fragment implements Observer{
	WeakReference<PaneTest> parent;
	ListView fileListView;
	ArrayList<Article> articleList = new ArrayList<Article>();
	MainPageHandler mainPageHandler = MainPageHandler.getInstance(null);
	@Override
	public void onAttach(Activity activity) {
		parent = new WeakReference<PaneTest>((PaneTest) activity);
		mainPageHandler.addObserver(this);
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_second_pane, container,
				false);
		fileListView = (ListView)v.findViewById(R.id.second_pane_listview);
		fileListView.setAdapter(new BaseAdapter() {
			
			@Override
			public View getView(int arg0, View arg1, ViewGroup arg2) {
				
				LayoutInflater inflater = (LayoutInflater)SecondPane.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				return getCell(inflater, arg0, arg1, arg2);

			}
			
			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Object getItem(int arg0) {
				return articleList.get(arg0);
			}
			
			@Override
			public int getCount() {
				return articleList.size();
			}
		});
		
		fileListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String url = articleList.get(arg2).getUrl();
				Fragment thirdPane = new ThirdPane(url);
				parent.get().push(thirdPane);
			}
		});
		
		return v;
	}
	
	public View getCell(LayoutInflater inflater, int position,
			View convertView, ViewGroup parent) {
		View v = inflater.inflate(R.layout.article_list_cell, null);
		TextView textView = (TextView) v.findViewById(R.id.article_name);
		Article article = mainPageHandler.getArticleList().get(position);
		textView.setText(article.getTitle());
		return v;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		articleList = mainPageHandler.getArticleList();
		Handler mainHandler = new Handler(Looper.getMainLooper());
		mainHandler.post(new Runnable() {
			public void run() {
				BaseAdapter adapter = (BaseAdapter) fileListView.getAdapter();
				adapter.notifyDataSetChanged();
			}
		});
		
	}
}
