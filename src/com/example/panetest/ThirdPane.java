package com.example.panetest;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.handler.ContentPageHandler;
import com.example.model.Content;
import com.example.model.WebConnectionOperation;

public class ThirdPane extends Fragment implements Observer {

	String previousLink;
	ListView contentListView;
	ArrayList<Content> contentList = new ArrayList<Content>();
	ContentPageHandler contentPageHandler = ContentPageHandler.getInstance();

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		WebConnectionOperation connection = WebConnectionOperation
				.getInstance();
		connection.sendReqest(previousLink);
		contentPageHandler.addObserver(this);
	}

	public ThirdPane(String url) {
		this.previousLink = url;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_third_pane, container,
				false);
		contentListView = (ListView) v.findViewById(R.id.third_pane_listview);
		contentListView.setAdapter(new BaseAdapter() {

			@Override
			public View getView(int arg0, View arg1, ViewGroup arg2) {
				LayoutInflater inflater = ThirdPane.this.getActivity()
						.getLayoutInflater();
				View v = inflater.inflate(R.layout.content_cell, null);
				TextView[] textViews = new TextView[6];
				textViews[0] = (TextView) v.findViewById(R.id.content1);
				TextView test = (TextView) v.findViewById(R.id.content1);
				textViews[1] = (TextView) v.findViewById(R.id.content2);
				textViews[2] = (TextView) v.findViewById(R.id.content3);
				textViews[3] = (TextView) v.findViewById(R.id.content4);
				textViews[4] = (TextView) v.findViewById(R.id.content5);
				textViews[5] = (TextView) v.findViewById(R.id.content6);

				int count = Math.min(contentList.size() - arg0 * 6, 6);
				for (int i = 0; i < count; i++) {

					String text = contentList.get(i + arg0 * 6).getChapter();
					textViews[i].setText(text);
				}
				return v;
			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getCount() {
				return contentList.size() / 6;
			}
		});

		return v;

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		contentList = contentPageHandler.getContentList();

		Handler mainHandler = new Handler(Looper.getMainLooper());
		mainHandler.post(new Runnable() {
			public void run() {
				BaseAdapter adapter = (BaseAdapter) contentListView
						.getAdapter();
				adapter.notifyDataSetChanged();
			}
		});
	}
	

}
