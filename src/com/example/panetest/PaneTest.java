package com.example.panetest;

import com.example.model.WebConnectionOperation;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class PaneTest extends ActionBarActivity {

	SlidingPaneLayout pane;
	WebConnectionOperation connection;

	public WebConnectionOperation getConnection() {
		return connection;
	}

	public void setConnection(WebConnectionOperation connection) {
		this.connection = connection;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pane_test);
		pane = (SlidingPaneLayout) findViewById(R.id.slidingpanelayout);
		pane.setPanelSlideListener(new SlidingPaneLayout.SimplePanelSlideListener() {

			@Override
			public void onPanelClosed(View panel) {
				switch (panel.getId()) {
				case R.id.fragment_secondpane:
					getFragmentManager().findFragmentById(R.id.fragment_firstpane).setHasOptionsMenu(false);
					getFragmentManager().findFragmentById(R.id.fragment_secondpane).setHasOptionsMenu(true);
					break;

				}
			}

			@Override
			public void onPanelOpened(View panel) {
				switch (panel.getId()) {
				case R.id.fragment_secondpane:
					getFragmentManager().findFragmentById(R.id.fragment_firstpane).setHasOptionsMenu(true);
					getFragmentManager().findFragmentById(R.id.fragment_secondpane).setHasOptionsMenu(false);
					break;

				default:
					break;
				}
			}

			@Override
			public void onPanelSlide(View panel, float slideOffset) {
				// TODO Auto-generated method stub
				super.onPanelSlide(panel, slideOffset);
			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pane_test, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void put(Fragment fragment, int viewResource) {
		Log.d("crash-try", "crash-try put in nav container");
		final FragmentManager fragmentManager = getFragmentManager();

		fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(viewResource, fragment);
		fragmentTransaction.commit();
	}

	public void replace(Fragment fragment) {
		put(fragment, R.id.nav_container);
	}
	
	public void push(Fragment fragment) {
//		if (navSlider.isOpen())
//			navSlider.closePane();

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.replace(R.id.nav_container, fragment);
		fragmentTransaction.addToBackStack(fragment.toString());
		fragmentTransaction.commit();
	}

	public void pop() {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.popBackStack();
	}

	@Override
	public void onBackPressed() {
		pop();
	}
}
