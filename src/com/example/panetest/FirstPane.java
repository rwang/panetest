package com.example.panetest;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.handler.MainPageHandler;
import com.example.model.Catagory;
import com.example.model.WebConnectionOperation;

public class FirstPane extends Fragment implements Observer {
	WebView webview;
	ListView menuList;
	WeakReference<PaneTest> parent;
	ArrayList<Catagory> catagoryList = new ArrayList<Catagory>();
	MainPageHandler mainPageHandler = MainPageHandler.getInstance(null);
	WebConnectionOperation connection;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		parent = new WeakReference<PaneTest>((PaneTest) activity);
		mainPageHandler.addObserver(this);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_first_pane, container,
				false);
		webview = (WebView) v.findViewById(R.id.dummywebview);
		connection = WebConnectionOperation.getInstance(
				getActivity(), webview);
		connection.sendReqest("/");
		menuList = (ListView) v.findViewById(R.id.menulist);
		menuList.setAdapter(new BaseAdapter() {
			private LayoutInflater inflater;

			@Override
			public View getView(int position, View convertView,
					ViewGroup rootView) {

				inflater = (LayoutInflater) FirstPane.this.getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				return getCell(inflater, position, convertView, rootView);
			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public Object getItem(int index) {
				return catagoryList.get(index);
			}

			@Override
			public int getCount() {
				return catagoryList.size();
			}
		});
		menuList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				{
					//if click on first row, just return;
					if(arg2 == 0)return;
					String name = catagoryList.get(arg2).getName();
					String url = catagoryList.get(arg2).getUrl();
					Log.d("FirstPane", name + ":" + url);
					connection.sendReqest(url);
				}
//				parent.get().replace((Fragment) new ThirdPane());
			}
		});
		return v;
	}



	public View getCell(LayoutInflater inflater, int position,
			View convertView, ViewGroup parent) {
		View v = inflater.inflate(R.layout.catagory_menu_cell, null);
		TextView textView = (TextView) v.findViewById(R.id.catagory_name);
		Catagory item = catagoryList.get(position);
		textView.setText(item.getName());
		v.setTag(catagoryList.get(position).getUrl());
		return v;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		Handler mainHandler = new Handler(Looper.getMainLooper());
		mainPageHandler.deleteObserver(this);
		mainHandler.post(new Runnable() {
			public void run() {
				catagoryList = mainPageHandler.getCatagoryList();
				BaseAdapter adapter = (BaseAdapter) menuList.getAdapter();
				adapter.notifyDataSetChanged();
			}
		});
	}
}
